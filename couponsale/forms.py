from django import forms
from django.utils import timezone
from .models import Sale, Coupon


class SaleForm(forms.Form):
    start = forms.DateTimeField(widget=forms.DateTimeInput, label="Sale Start Datetime ( yyyy-mm-dd hh-mm-ss )")
    interval = forms.IntegerField(label="Coupon Code Interval")
    coupons_file = forms.FileField(label="Coupons Text File (line separated)")

    def create_sale(self):
        # First Delete Old Sale Record
        coupons = Coupon.objects.all()
        for c in coupons:
            c.delete()
        old_sales = Sale.objects.all()
        for s in old_sales:
            s.delete()
        # Now create a New Sale Record
        new_sale = Sale.objects.create(
            start=self.cleaned_data['start'],
            interval=self.cleaned_data['interval'],
            count=0,
            last_request=self.cleaned_data['start'] - timezone.timedelta(days=365))
        coupons_data = self.cleaned_data['coupons_file'].read().decode('utf-8')
        coupon_code_list = coupons_data.splitlines()
        for code in coupon_code_list:
            coupon = Coupon.objects.create(code=code)
