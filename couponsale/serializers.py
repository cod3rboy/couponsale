from rest_framework import serializers
from .models import Coupon, Sale


class CouponSerializer(serializers.ModelSerializer):
    left = serializers.SerializerMethodField('get_coupons_left')

    def get_coupons_left(self, couponObject):
        total = Coupon.objects.all().count()
        count = Sale.objects.first().count
        return total-count

    class Meta:
        model = Coupon
        fields = ['code', 'left']


class SaleSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField('get_total_coupons')

    def get_total_coupons(self, sale_object):
        return Coupon.objects.all().count()

    class Meta:
        model = Sale
        fields = ['start', 'interval', 'count', 'total']
