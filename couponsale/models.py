from django.db import models


class Coupon(models.Model):
    code = models.CharField(max_length=25, blank=False, null=False)

    def __str__(self):
        return self.code


class Sale(models.Model):
    start = models.DateTimeField(blank=False, null=False)
    interval = models.IntegerField(blank=False, null=False)
    count = models.IntegerField(blank=False, null=False)
    last_request = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return 'Coupon Sale at {0}'.format(self.start)
