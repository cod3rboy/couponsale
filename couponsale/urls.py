from django.urls import path
from . import views

urlpatterns = [
    path('next-code/', views.CouponView.as_view(), name='next-code'),
    path('sale-info/', views.SaleView.as_view(), name='sale-info'),
    path('create-sale/', views.CreateSaleView.as_view(), name='create-sale'),
]
