from django.utils import timezone
from django.urls import reverse_lazy, reverse
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.views import APIView
from .models import Coupon, Sale
from .serializers import CouponSerializer, SaleSerializer
from .forms import SaleForm
from rest_framework.response import Response
from rest_framework import status


class CouponView(APIView):
    """
    Return next coupon
    """
    RESPONSE_NO_SALE = {"detail": "No ongoing sale found."}
    RESPONSE_SALE_NOT_STARTED = {"detail": "Sale is not yet started."}
    RESPONSE_WAIT_FOR_INTERVAL = {"detail": ""}
    RESPONSE_SALE_ENDED = {"detail": "No more coupon available. Sale has ended."}

    def get(self, request):
        sale = Sale.objects.first()
        # Check whether sale exists
        if sale is None:
            return Response(self.RESPONSE_NO_SALE, status=status.HTTP_200_OK)

        # Check whether sale started
        now = timezone.now()
        if sale.start > now:
            return Response(self.RESPONSE_SALE_NOT_STARTED, status=status.HTTP_200_OK)

        # Check whether sale ended
        coupons = Coupon.objects.all().order_by('id')
        if sale.count == coupons.count():
            # All Coupon codes are used
            return Response(self.RESPONSE_SALE_ENDED, status=status.HTTP_404_NOT_FOUND)

        # Check whether request timing is right
        interval = timezone.timedelta(seconds=sale.interval)
        next_request_time = sale.last_request + interval
        if next_request_time > now:
            diff = next_request_time - now
            wait_secs = 24 * 60 * 60 * diff.days + diff.seconds
            self.RESPONSE_WAIT_FOR_INTERVAL["detail"] = "You have to wait for %d seconds before requesting next coupon." % wait_secs
            self.RESPONSE_WAIT_FOR_INTERVAL["secs"] = str(wait_secs)
            prev_coupon = coupons[sale.count - 1]
            self.RESPONSE_WAIT_FOR_INTERVAL["code"] = prev_coupon.code
            self.RESPONSE_WAIT_FOR_INTERVAL["left"] = coupons.count() - sale.count
            return Response(self.RESPONSE_WAIT_FOR_INTERVAL, status=status.HTTP_400_BAD_REQUEST)

        # Return the next coupon code
        next_coupon = coupons[sale.count]
        sale.count += 1
        sale.last_request = now
        sale.save()
        return Response(CouponSerializer(next_coupon).data, status=status.HTTP_200_OK)


class SaleView(APIView):
    """
    Return Sale Information
    """
    RESPONSE_NO_SALE = {"detail": "No ongoing sale found."}

    def get(self, request):
        sale = Sale.objects.first()
        if sale is None:
            return Response(self.RESPONSE_NO_SALE, status=status.HTTP_200_OK)
        sale_info = SaleSerializer(sale).data
        return Response(sale_info, status=status.HTTP_200_OK)


class CreateSaleView(LoginRequiredMixin, FormView):
    """
    Create Sale of Coupons
    """
    login_url = '/admin/login'
    template_name = 'couponsale/create_sale.html'
    form_class = SaleForm
    success_url = reverse_lazy('sale-info')

    def form_valid(self, form):
        form.create_sale()
        return super().form_valid(form)
