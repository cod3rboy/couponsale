from django.contrib import admin
from .models import Coupon, Sale

admin.site.register(Coupon)
admin.site.register(Sale)
