from django.apps import AppConfig


class CouponsaleConfig(AppConfig):
    name = 'couponsale'
