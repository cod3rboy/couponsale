import uuid
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIClient
from .models import Sale, Coupon
from .views import SaleView, CouponView
from .serializers import SaleSerializer, CouponSerializer


class SaleTestCase(TestCase):

    def setUp(self):
        self.api_client = APIClient()
        self.coupons = list()
        # Create 10 coupon records
        for i in range(10):
            coupon = Coupon.objects.create(code=uuid.uuid4().hex[:23].upper())
            self.coupons.append(coupon)

    def test_sale_endpoint_exists(self):
        response = self.client.get(reverse('sale-info'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_method_disallowed(self):
        response = self.client.post(reverse('sale-info'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_sale_not_found(self):
        # First delete all sales records
        sale_records = Sale.objects.all()
        for sale in sale_records:
            sale.delete()
        response = self.api_client.get(reverse('sale-info'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, SaleView.RESPONSE_NO_SALE['detail'])

    def test_sale_found(self):
        # First create a sale record
        sale = Sale.objects.create(
            start=timezone.now(),
            count=0,
            last_request=timezone.now(),
            interval=15)
        serialized_data = SaleSerializer(sale).data
        response = self.api_client.get(reverse('sale-info'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)

    def test_sale_info_update(self):
        # First create a sale record
        sale = Sale.objects.create(
            start=timezone.now(),
            count=0,
            last_request=timezone.now() - timezone.timedelta(seconds=16),
            interval=15
        )
        # First visit at `next-code` url to update sale info
        response = self.api_client.get(reverse('next-code'))
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Ensure first coupon is returned as count is initially 0 in sale info
        self.assertEqual(response.data, CouponSerializer(self.coupons[0]).data)
        # Now ensure sale info is updated
        response = self.api_client.get(reverse('sale-info'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count']-1, sale.count)
        updated_sale = Sale.objects.get(pk=sale.pk)
        self.assertGreater(updated_sale.last_request, sale.last_request)


class CouponTestCase(TestCase):

    def setUp(self):
        self.api_client = APIClient()
        self.coupons = list()
        # Create 10 coupon records
        for i in range(10):
            coupon = Coupon.objects.create(code=uuid.uuid4().hex[:23].upper())
            self.coupons.append(coupon)

    def test_coupon_endpoint_exists(self):
        response = self.client.get(reverse('next-code'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_method_disallowed(self):
        response = self.client.post(reverse('next-code'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_sale_not_found(self):
        # First delete all sales records
        sale_records = Sale.objects.all()
        for sale in sale_records:
            sale.delete()
        response = self.api_client.get(reverse('next-code'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, CouponView.RESPONSE_NO_SALE['detail'])

    def test_sale_not_started(self):
        # Create a sale record first
        sale = Sale.objects.create(
            start=timezone.now() + timezone.timedelta(days=1), # Start time in future
            interval=10,
            count=0,
            last_request=timezone.now())
        response = self.client.get(reverse('next-code'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, CouponView.RESPONSE_SALE_NOT_STARTED['detail'])

    def test_sale_ended(self):
        # Create a sale record first
        sale = Sale.objects.create(
            start=timezone.now() - timezone.timedelta(days=1), # Start time in past
            interval=10,
            count=len(self.coupons),
            last_request=timezone.now() - timezone.timedelta(hours=1))
        response = self.client.get(reverse('next-code'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_sale_request_wait(self):
        # Create a sale record first
        sale = Sale.objects.create(
            start=timezone.now() - timezone.timedelta(hours=1), # Start time in past
            interval=10, # 10 seconds interval before next request
            count=2,
            last_request=timezone.now() - timezone.timedelta(seconds=5) # last request was 5 seconds ago
        )
        response = self.client.get(reverse('next-code'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_next_coupon_code(self):
        # Create a sale record first
        sale = Sale.objects.create(
            start=timezone.now() - timezone.timedelta(minutes=10), # Start time in past
            interval=10,
            count=2,
            last_request=timezone.now() - timezone.timedelta(seconds=10)
        )
        response = self.api_client.get(reverse('next-code'))
        serialized_data = CouponSerializer(self.coupons[sale.count]).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)
